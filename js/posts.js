var posts = [

{
	authorName: "Eva",

	authorPhoto: "img/avatars/eva.png",

	authorId: 1,

	postId: 1,

	postDate: '10.08.2016',

	topic: "Will insulin make my patient gain weight",

	relatedCount: 1,

	involvedCount: 6,

	conversationCount: 3,

	generalActivitiesCount: 7,

	activities : [
	{	
		authorName: "Waweru",
		authorId: 2,
		authorPhoto: "img/avatars/person1.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png",
		type: "commented"
	},
	{	
		authorName: "David",
		authorId: 4,
		authorPhoto: "img/avatars/david.png",
		type: "commented"
	},
	{	
		authorName: "Joseph",
		authorId: 5,
		authorPhoto: "img/avatars/joseph.png",
		type: "answered"
	},
	]
},

{
	authorName: "Andrew",

	authorPhoto: "img/avatars/andrew.png",

	authorId: 5,

	postId: 2,

	postDate: '09.08.2016',

	topic: "Vegan diet in diabetes treatment",

	relatedCount: 2,

	involvedCount: 9,

	conversationCount: 5,

	generalActivitiesCount: 10,

	activities : [
	{	
		authorName: "David",
		authorId: 4,
		authorPhoto: "img/avatars/david.png",
		type: "commented"
	},
	{	
		authorName: "Joseph",
		authorId: 5,
		authorPhoto: "img/avatars/joseph.png" ,
		type: "commented"
	},
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png" ,
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "answered"
	}
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '07.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 3,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 7,

	postId: 4,

	postDate: '06.08.2016',

	topic: "Another one bites the dust",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 1,

	activities : [
	{	
		authorName: "Waweru",
		authorId: 8,
		authorPhoto: "img/avatars/person1.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/person1.png" ,
		type: "commented"
	},
	{	
		authorName: "Waweru",
		authorId: 8,
		authorPhoto: "img/avatars/person1.png" ,
		type: "answered"
	}
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '05.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 2,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '04.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 5,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '03.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 4,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '02.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 4,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '01.08.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 4,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '21.07.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 4,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '20.07.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 4,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

{
	authorName: "Joseph",

	authorPhoto: "img/avatars/joseph.png",

	authorId: 5,

	postId: 3,

	postDate: '19.07.2016',

	topic: "Vegan diet to stop diabetes progress",

	relatedCount: 5,

	involvedCount: 4,

	conversationCount: 0,

	generalActivitiesCount: 2,

	activities : [
	{	
		authorName: "Halima",
		authorId: 6,
		authorPhoto: "img/avatars/halima.png",
		type: "commented"
	},
	{	
		authorName: "Patricia",
		authorId: 3,
		authorPhoto: "img/avatars/patricia.png" ,
		type: "commented"
	},
	{	
		authorName: "Eva",
		authorId: 1,
		authorPhoto: "img/avatars/eva.png" ,
		type: "answered"
	},
	]
},

]