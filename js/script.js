(function () {

  $('.search-icon').on('click', function(){
    $('.bottom-bar').slideToggle(300);
  });

  // HELLO:
  // This function adds the appropriate button containers depending on the fact if there are any comments or not
  // I'm an Angular newbie so I couldn't find the appropriate way to do it with this framework
  $('.answer').each(function(){
    if($(this).find('ul').length) {
      $(this).addClass('has-comment').find('ul li:last-child').addClass('last-comment')
      $(this).append('<div class="comment-button"><p><span>Continue</span> discussion</p></div>');
    } else {
      $(this).addClass('no-comment').append('<div class="comment-button"><p><span>Comment</span></p></div>');
    }
  });

  // angular
  var myApp = angular.module('forum', []);

  myApp.controller('mainCtrl', function($scope){
    // assign posts array
    $scope.posts = posts;
    
    // operate non-expanded question column view and number
    $scope.activityTypeCheck = function(value){
      if(value=="answered"){
        return "column activity activity--answer"
      }else {
        return "column activity mobile-hide";
      };
    };

    $scope.hideColumnCheck = function(value){
      if(value<=5){
        return "column activities hide"
      }else {
        return "column activities";
      };
    };

    // user profile pop
    $scope.showUserProfile = function() {
      // var id = value;
      $('.profile-wrap').fadeIn(300);
      document.body.style.overflow = "hidden";
    };

    $scope.hideUserProfile = function() {
      $('.profile-wrap').fadeOut(300);
      document.body.style.overflow = "visible";
    };

    // load more
    var itemsShown = 3;
    var pagesShown = 1
    $scope.itemsLimit = function() {
      return pagesShown * itemsShown;
    };

    $scope.hasMoreItemsToShow = function() {
      return pagesShown < ($scope.posts.length / itemsShown);
    };

    $scope.showMoreItems = function() {
      pagesShown = pagesShown + 1;         
    };

    // search
    $scope.q="";
    
    $scope.submit=function(value){
      $scope.q = value;
      console.log($scope.q);
    }

    // sortBy
    $scope.sortPosts = '-postDate';

    $scope.changeSort=function(value) {
      $scope.sortPosts = value;
      console.log($scope.sortPosts);
    }
  });

  // memo
  var memo = angular.module('memo', []);

  memo.filter('makePositive', function() {
    return function(num) { return Math.abs(num); }
  });

  memo.controller('memoCtrl', function($scope){
    var likeboxes = [
    {
      initialNumber: 0
    }
    ];

    $scope.likeboxes = likeboxes;
    var value = 0;

    $scope.incrementLikes = function(likebox) {
      likebox.initialNumber++;
      value++;
      console.log(value);
      if(value > 0) {;
        $('.vote-result .result').text('upvotes')
      } else if( value == 0) {
        $('.vote-result .result').text('votes');
      };
    };

    $scope.incrementDislikes = function(likebox) {
      likebox.initialNumber--;
      value--;
      console.log(value);
      if(value < 0) {
        $('.vote-result .result').text('downvotes');
      } else if( value == 0) {
        $('.vote-result .result').text('votes');
      };
    };

  });



})();
